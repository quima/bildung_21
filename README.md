# bildung_21 – Bildungskonzept(e) für eine gemeinwohlorientierte Schulausbildung in einer digitale Welt

## Warum das alles?

Die Gesellschaft wandelt sich, Kompetenzen in Mediennutzung und Informatik
werden immer wichtiger, während sich durch neue Technologien neue Möglichkeiten,
aber auch neue Risiken entwickeln.  Um diesen Veränderungen Rechnung zu tragen,
sollen hier ein paar Ideen gesammelt werden, welche in konstruktiver Weise
versuchen wollen, mögliche Wege aufzuzeigen.

## Struktur

- [braindump.md](./braindump.md): Sammlung loser Ideen
- [overview.md](./overview.md): Übersicht über die Konzeptvorschläge

## Motivation: Informatik als „das wichtigste Randfach überhaupt“

_TODO_: das ist nur ein Braindump, muss noch ausformuliert werden.

Warum gerade Informatik speziell betrachten?  Gründe:
- Durchzieht mittlerweile alle Bereiche des täglichen Lebens, ist für viele
  Arbeitsbereiche unentbehrlich
  - Einige sind dem dann auch einfach wegen fehlendem Verständnis „ausgeliefert“
- Ausbildung wird immer noch sehr isoliert von anderen Fächern betrachtet, wenig
  Überschneidung mit anderen Fächern (auch wenn man jetzt mittlerweile überall
  als Projekt Filmchen drehen muss, aber ist das schon Informatik?)
- Fokus bei Ausbildung auf – überspitzt formuliert – „Anwenderschulung für
  M$ Office“, anstatt auf Verständnis von Konzepten der Informatik und
  Betrachtung von Möglichkeiten für deren Umsetzung
  - Bekommt man in seinem Schulleben eigentlich mal erklärt, wie das Internet
    grundlegend funktioniert, oder ist die [Sendung mit der
    Maus](https://iteroni.com/watch?v=fpqhjEtznVk) immer noch das beste Medium
    dafür?
- Grundlegende Bedeutung von Informatik für die Gesellschaft wird selten
  ganzheitlich betrachtet, obwohl Informatik omnipräsent ist: von „neuen Medien“
  bis hin zur kritischen Infrastruktur
  - Wird eventuell eine kritischere Haltung zu Social Media gestärkt durch ein
    besseres Verständnis von Informatik?
