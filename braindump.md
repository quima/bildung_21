[ NB: abgehakte Punkte sind irgendwie schon in [overview.md](./overview.md) mit eingeflossen ]

# Allgemeinbildende Schulen
- [ ] Lehrer:innen wechsel alle 2 Jahre?
- [ ] Qualitätskontrolle?
- [X] freie Lerninhalte (OER)
- [X] freie Software
- [X] Informatik als Interdiszliplinäres Fach
- [ ] verlängerung der Regelschulzeit
- [X] Technik-/Medienkompetenz durch Evolution...
... In der Grundschule das Gehäuse für ein Notebook bauen
... In der Oberschule die Hardware und Software
... offene Hardware mit freier Software
... welche Anforderungen muss das Gerät erfüllen?
- [X] diverseres Personal in Schule (Kooperation mit Wirtschaft)
- [ ] mehr Schulsozialarbeit
- [X] Schule als Ort des Lebens begreifen
- [X] Schule Nachmittags als Stadtteilhaus nutzen
- [X] Schule als Gesamtgesellschaftlichen Ort der Bildung betrachten
- [X] Dokumentation von Lernproblemen (bspw. Wiki)
    -> erstellung eigener Bildungsinhalte (Wikipedia-Prinzip)
- [X] Inklusive Schule denken
- [X] Stadtteil Schule möglich?
- [ ] Lernumgebung
    - [ ] Fachräume/ freie Lernräume
    - [ ] möglichst modular
    - [ ] Licht, Sitzmöglichkeiten, tische... Wie muss es sein damit es möglichst individuell anpassbar ist?
- [ ] Lebenslanges Lernen
- [ ] Abschaffung Noten (zumindest in Sport, Musik und Kunst)
- [ ] keine Sportfeste
- [ ] keine Skilager
- [ ] FreiDay oder RausDay (frei-day.org)
- [ ] Orte der Bildung, >Weiterentwicklung mit Umfeld vernetzen
- [ ] Experten von "Außen" an die Schulen
- [ ] umfangreiche digitale Bildung ab Kindergarten
- [ ] Ausstattung mt digitalen Geräten, genug, vielfältig, gute Wartung
- [ ] Förderung von Open Source
- [ ] Prinzipien der Teilens (OER)
- [ ] Schaffung von Zentren digitaler Kompetenz (Schule, Gemeinde, Kreis) mit Koordinierter Weterbildung (wie Elternräte)
- [ ] Gesamtschule
- [ ] Kein Religionsunterricht (kann Kirche machen), alle machen Ethik
- [ ] Förderung von Demokratie
    - Aula (aula.de)
    - Echte Mitbestimmung für Schüler:innen an Schulen



