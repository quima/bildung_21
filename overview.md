# Ideen für eine gemeinwohlorientierte Schulausbildung in einer digitalen Welt

Was folgt ist der Versuch, aus [Ideen](./braindump.md) ein paar Kernthemen
herauszulösen und hier umfassender darzustellen.

## Schule als gesamtgesellschaftlicher Bildungsort

- Schule als Gesamtgesellschaftlichen Ort der Bildung betrachten
- Schule als Ort des Lebens begreifen
- Schule Nachmittags als Stadtteilhaus nutzen
- Inklusive Schule denken
- Stadtteil Schule möglich?
- diverseres Personal in Schule (Kooperation mit Wirtschaft)

## Informatik als interdisziplinäres Fach leben

- Informatik als Interdiszliplinäres Fach
- Dokumentation von Lernproblemen (bspw. Wiki)
    -> erstellung eigener Bildungsinhalte (Wikipedia-Prinzip)
- Technik-/Medienkompetenz durch Evolution...
... In der Grundschule das Gehäuse für ein Notebook bauen
... In der Oberschule die Hardware und Software
... offene Hardware mit freier Software
... welche Anforderungen muss das Gerät erfüllen?

## Freier Zugang zu Software und Lehrmaterialen

- Informatik kritisch für die digitale Gesellschaft, Ausschluss von Zugang zu
  „digitalen Kernelementen“ (Software, Bildung) bedeutet daher automatisch
  Ausschluss von gesellschaftlicher Teilhabe
- freie Lerninhalte (OER)
- freie Software
